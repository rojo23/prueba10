﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace pedidosprueba
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                 name: "raiz",
                 url: "",
                 defaults: new { controller = "OrdenPedido", action = "Index" },
                 constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
                 );

            routes.MapRoute(
                name: "ordenpedido",
                url: "ordenpedido",
                defaults: new { controller = "OrdenPedido", action = "Index" },
                constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
                );

            routes.MapRoute(
                 name: "nueva_ordenpedido",
                 url: "ordenpedido/nueva",
                 defaults: new { controller = "OrdenPedido", action = "Nueva" },
                 constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
                 );

            routes.MapRoute(
                 name: "editar_ordenpedido",
                 url: "ordenpedido/{id}/editar",
                 defaults: new { controller = "OrdenPedido", action = "Editar" },
                 constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
                 );

            routes.MapRoute(
                 name: "ver_ordenpedido",
                 url: "ordenpedido/{id}/ver",
                 defaults: new { controller = "OrdenPedido", action = "Ver" },
                 constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
                 );
            //Rutas del cliente

            routes.MapRoute(
            name: "clientes",
            url: "clientes",
            defaults: new { controller = "Clientes", action = "Index" },
            constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
          );

            routes.MapRoute(
            name: "nuevo_cliente",
            url: "clientes/nuevo",
            defaults: new { controller = "Clientes", action = "Nuevo" },
            constraints: new { httpMethod = new HttpMethodConstraint(new string[] { "GET" }) }
            );

        }
    }
}