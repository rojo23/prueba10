﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pedidosprueba.Controllers
{
    public class OrdenPedidoController : Controller
    {
        //
        // GET: /OrdenPedido/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Nueva()
        {
            return View();
        }

        public ActionResult Editar(int id)
        {
            ViewBag.id = id;
            return View();
        }

        public ActionResult Ver(int id)
        {
            ViewBag.id = id;
            return View();
        }
    }
}
